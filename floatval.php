<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        /*
         * floatval=get float value of a variable
         * float floatval(mixed$var)
         */
        
         $var = '122.34343The';
         $float_value_of_var = floatval($var);
         echo $float_value_of_var; // 122.34343
        ?>
    </body>
</html>
